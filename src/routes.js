import React, { Component } from "react";
import App from "./App";

class AppRoutes extends Component {
  render() {
    return (
      <div>
        <App />
      </div>
    );
  }
}

export default AppRoutes;
