import React, { Component } from "react";
import fire from "../config/Fire";
import "./Login.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      email: "",
      password: ""
    };
    this.signup = this.signup.bind(this);
  }

  login(e) {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password);
  }

  signup(e) {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    return (
      <div className="container">
        <form>
          <div className="content">
            <label>Email Address</label>
            <input
              value={this.state.email}
              onChange={this.handleChange}
              type="email"
              name="email"
              placeholder="Enter Email"
            />
          </div>
          <div className="content">
            <label>Password</label>
            <input
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
              name="password"
              placeholder="Password"
            />
          </div>
          <div className="botons">
            <button type="submit" onClick={this.login}>
              {" "}
              Login{" "}
            </button>
            <button type="submit" onClick={this.signup}>
              {" "}
              Register{" "}
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;
