import firebase from "firebase";
import 'firebase/firestore';
import 'firebase/app';

const config = {
  apiKey: "AIzaSyCju2xeJf5tXCo0ZofnIia-R_2B9zfBA7A",
  authDomain: "testreact-71907.firebaseapp.com",
  databaseURL: "https://testreact-71907.firebaseio.com",
  projectId: "testreact-71907",
  storageBucket: "testreact-71907.appspot.com",
  messagingSenderId: "834798534697",
  appId: "1:834798534697:web:b382460a5ffc0d70"
};

// Initialize Firebase
const fire = firebase.initializeApp(config);

export default fire;
