import firebase from "firebase";

// Initialize Firebase
let db = firebase.firestore();
db.settings({timestampsInSnapshots:true});

export default db;