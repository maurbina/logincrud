import React, { Component } from "react";
import fire from "../config/Fire";
import ListaTareas from "./ListaDeTareas/ListaDeTareas";
import "./Home.css";
import db from "../config/Db";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Switch } from "react-router-dom";

class Home extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.state = {
      ElementosListas: [],
      text: "",
      tareas: [],
      edit: false,
      id: "",
      count: 0,
      boll: false
    };
  }

  componentDidMount() {
    db.collection("tareas").onSnapshot(
      snapShots => {
        this.setState({
          tareas: snapShots.docs.map(doc => {
            return { id: doc.id, data: doc.data() };
          })
        });
      },
      error => {
        console.log(error);
      }
    );
  }

  logout() {
    fire.auth().signOut();
    //props.hendleLogout(this.state.boll);
  }

  EstadoCambio = e => {
    this.setState({ text: e.target.value });
  };

  delete = id => {
    db.collection("tareas")
      .doc(id)
      .delete();
  };

  EstadoSubmit = e => {
    e.preventDefault();
    if (this.state.text !== "") {
      let nextElement = this.state.ElementosListas.concat(this.state.text);
      let nextText = "";
      this.setState({
        ElementosListas: nextElement,
        text: nextText
      });
    }
  };

  //new version CRUD
  changeValue = e => {
    this.setState({
      inputValue: e.target.value
    });
  };

  getTodo = id => {
    let docRef = db.collection("tareas").doc(id);
    docRef
      .get()
      .then(doc => {
        if (doc.exists) {
          this.setState({
            text: doc.data().nombre,
            edit: true,
            id: doc.id
          });
        } else {
          console.log("el documento no existe");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  update = () => {
    const { id, text } = this.state;
    this.setState({
      edit: false
    });
    db.collection("tareas")
      .doc(id)
      .update({
        nombre: text
      })
      .then(() => {
        console.log("actualizado");
      })
      .catch(error => {
        console.log(error);
      });
  };

  action = () => {
    const { text, edit } = this.state;
    if (!edit && text !== "") {
      db.collection("tareas")
        .add({
          nombre: text
        })
        .then(() => {
          console.log("agregado");
        })
        .catch(() => {
          console.log("error");
        });
    } else if (!edit && text === "") {
      console.log("");
    } else {
      this.update();
    }
  };

  render() {
    const { edit } = this.state;
    return (
      <div>
        <Router>
          <Switch>
            <div>
              <Link to="/">
                <button className="logout" onClick={this.logout}>
                  LogOut
                </button>
              </Link>
            </div>
          </Switch>
        </Router>

        <div className="contain">
          <h3>Lista de Tareas</h3>
          <form onSubmit={this.EstadoSubmit}>
            <input
              placeholder="Agregar a la lista"
              onChange={this.EstadoCambio}
              value={this.state.text}
            />
            <button className="butNewUser" onClick={this.action}>
              {!edit ? "Agregar a la lista " : "Actualizar"}
            </button>
          </form>
          <div className="containerChange">
            <p>{this.state.text}</p>
          </div>

          <ListaTareas
            ElementosListasNew={this.state.ElementosListas}
            delete={this.delete}
            tareas={this.state.tareas}
            getTodo={this.getTodo}
          />
        </div>
      </div>
    );
  }
}

export default Home;
