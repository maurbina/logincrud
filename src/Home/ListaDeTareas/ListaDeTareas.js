import React, { Component } from "react";
import "./ListaTareas.css";

class ListaTareas extends Component {
  render() {
    const { tareas } = this.props;
    return (
      <ul className="sortable">
        {tareas && tareas !== "undefined"
          ? tareas.map((tarea, key) => (
              <li key={key} className="listTask">
                <button onClick={() => this.props.getTodo(tarea.id)}>
                  Edit
                </button>
                <div>{tarea.data.nombre}</div>
                <button onClick={() => this.props.delete(tarea.id)}>X</button>
              </li>
            ))
          : ""}
      </ul>
    );
  }
}

export default ListaTareas;
