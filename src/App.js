import React, { Component } from "react";
import fire from "./config/Fire";
import Home from "./Home/Home";
import Login from "./Auth/Login";
import { Route, Switch, Redirect } from "react-router-dom";

class App extends Component {
  state = {
    user: {},
    boll: false
  };

  componentDidMount() {
    this.authListener();
  }

  authListener() {
    fire.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ boll: true });
      } else {
        this.setState({ boll: false });
        return <Redirect to="/" />;
      }
    });
  }

  render() {
    function PrivateRoute({ component: Component, authed, ...rest }) {
      return (
        <Route
          {...rest}
          render={props =>
            authed === false ? (
              <Component {...rest} />
            ) : (
              <Redirect
                to={{ pathname: "/", state: { from: props.location } }}
              />
            )
          }
        />
      );
    }

    return (
      <div className="App">
        {this.state.boll ? (
          <Switch>
            <Route path="/home" component={Home} />
            <Redirect to="/home" />
          </Switch>
        ) : (
          <Switch>
            <PrivateRoute authed={this.state.boll} path="/" component={Login} />
            {console.log("logout inside")}
          </Switch>
        )}
      </div>
    );
  }
}

export default App;
